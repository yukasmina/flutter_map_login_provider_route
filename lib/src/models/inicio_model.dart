// To parse this JSON data, do
//
//     final loginModel = loginModelFromMap(jsonString);

import 'dart:convert';

class LoginModel {
  LoginModel({
    this.en,
    this.m,
    this.auth,
    this.cliente,
  });

  int en;
  String m;
  String auth;
  Cliente cliente;

  factory LoginModel.fromJson(String str) =>
      LoginModel.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory LoginModel.fromMap(Map<String, dynamic> json) => LoginModel(
        en: json["en"] == null ? null : json["en"],
        m: json["m"] == null ? null : json["m"],
        auth: json["auth"] == null ? null : json["auth"],
        cliente:
            json["cliente"] == null ? null : Cliente.fromMap(json["cliente"]),
      );

  Map<String, dynamic> toMap() => {
        "en": en == null ? null : en,
        "m": m == null ? null : m,
        "auth": auth == null ? null : auth,
        "cliente": cliente == null ? null : cliente.toMap(),
      };
}

class Cliente {
  Cliente({
    this.isPortero,
    this.idCliente,
    this.isFacebook,
    this.isApple,
    this.celular,
    this.correo,
    this.nombres,
    this.apellidos,
    this.cedula,
    this.pasaporte,
    this.cambiarClave,
    this.codigoPais,
    this.celularValidado,
    this.correoValidado,
    this.codigoValidado,
  });

  int isPortero;
  int idCliente;
  int isFacebook;
  int isApple;
  String celular;
  String correo;
  String nombres;
  String apellidos;
  dynamic cedula;
  dynamic pasaporte;
  int cambiarClave;
  String codigoPais;
  int celularValidado;
  int correoValidado;
  int codigoValidado;

  factory Cliente.fromJson(String str) => Cliente.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Cliente.fromMap(Map<String, dynamic> json) => Cliente(
        isPortero: json["isPortero"] == null ? null : json["isPortero"],
        idCliente: json["idCliente"] == null ? null : json["idCliente"],
        isFacebook: json["isFacebook"] == null ? null : json["isFacebook"],
        isApple: json["isApple"] == null ? null : json["isApple"],
        celular: json["celular"] == null ? null : json["celular"],
        correo: json["correo"] == null ? null : json["correo"],
        nombres: json["nombres"] == null ? null : json["nombres"],
        apellidos: json["apellidos"] == null ? null : json["apellidos"],
        cedula: json["cedula"],
        pasaporte: json["pasaporte"],
        cambiarClave:
            json["cambiarClave"] == null ? null : json["cambiarClave"],
        codigoPais: json["codigoPais"] == null ? null : json["codigoPais"],
        celularValidado:
            json["celularValidado"] == null ? null : json["celularValidado"],
        correoValidado:
            json["correoValidado"] == null ? null : json["correoValidado"],
        codigoValidado:
            json["codigoValidado"] == null ? null : json["codigoValidado"],
      );

  Map<String, dynamic> toMap() => {
        "isPortero": isPortero == null ? null : isPortero,
        "idCliente": idCliente == null ? null : idCliente,
        "isFacebook": isFacebook == null ? null : isFacebook,
        "isApple": isApple == null ? null : isApple,
        "celular": celular == null ? null : celular,
        "correo": correo == null ? null : correo,
        "nombres": nombres == null ? null : nombres,
        "apellidos": apellidos == null ? null : apellidos,
        "cedula": cedula,
        "pasaporte": pasaporte,
        "cambiarClave": cambiarClave == null ? null : cambiarClave,
        "codigoPais": codigoPais == null ? null : codigoPais,
        "celularValidado": celularValidado == null ? null : celularValidado,
        "correoValidado": correoValidado == null ? null : correoValidado,
        "codigoValidado": codigoValidado == null ? null : codigoValidado,
      };
}
