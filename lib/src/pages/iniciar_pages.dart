import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:app_provider/src/models/inicio_model.dart';
import 'package:app_provider/src/providers/inicio_provider.dart';

class IniciarPages extends StatelessWidget {
  final LoginModel inicioModel = new LoginModel();
  final inicioProvider = new InicioProvider();
  final TextEditingController usuarioController = new TextEditingController();
  final TextEditingController claveController = new TextEditingController();

  void setValues(LoginModel loginModel) async {
    print('Guardado los datos');
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString('usuario', loginModel.toJson());
  }

  void getValues(BuildContext context) async {
    print('Presenta los datos');
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String usuario = sharedPreferences.getString('usuario');
    _mostrarAlertDanger(context, usuario);
    //print("Usuario: $usuario");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Login"),
        ),
        body: ListView(
          padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
          children: <Widget>[
            _createEmail(),
            Divider(),
            _createPassword(),
            Divider(),
            _save(context),
          ],
        ));
  }

  _createEmail() {
    return TextFormField(
      controller: usuarioController,
      //initialValue: inicioModel.cliente.correo,
      //keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          hintText: 'Usuario',
          labelText: 'Usuario',
          helperText: 'Solo es el nombre',
          suffixIcon: Icon(Icons.phone_android),
          icon: Icon(Icons.phone)),
    );
  }

  _createPassword() {
    return TextFormField(
      //initialValue: inicioModel.cliente. ,

      controller: claveController,
      //obscureText: true,
      decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(20.0)),
          hintText: 'Clave',
          labelText: 'Clave',
          //helperText: 'Solo es el nombre',
          suffixIcon: Icon(Icons.lock_open),
          icon: Icon(Icons.lock)),
    );
  }

  _save(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          child: RaisedButton(
            onPressed: () async {
              final LoginModel login = await inicioProvider.consultaDatos(
                  usuarioController.text.toString(),
                  claveController.text.toString());
              print("Mensaje de llegada");
              if (login != null) {
                if (login.en == 1) {
                  setValues(login);
                  _mostrarAlert(context, login);
                } else {
                  _mostrarAlertDanger(context, login.m);
                }
              } else {
                _mostrarAlertDanger(
                    context, "Usuario y/o contraseñas incorrectas");
              }

              //print(login.cliente.apellidos);
              //_presentacioDatos();
            },
            child: Text("Login"),
            color: Colors.blue,
            textColor: Colors.white,
          ),
        ),
        Container(
            child: RaisedButton(
          onPressed: () async => getValues(context),
          child: Text("Presentar"),
          color: Colors.blueAccent,
          textColor: Colors.white,
        )),
      ],
    );
  }

  _presentacioDatos() {
    print("________No usoooooooooooooooooooooooooooooooooo___________");
    return FutureBuilder(
      future: inicioProvider.consultaDatos(
          usuarioController.text.toString(), claveController.text.toString()),
      builder: (BuildContext context, AsyncSnapshot<LoginModel> snapshot) {
        print("Mensaje de llegada de datos");
        if (snapshot.hasData) {
          final datos = snapshot.data;
          print(datos);
          //return _mostrarAlert(context, 'Datos', datos);
          return Container(
            child: Text(datos.auth),
          );
        } else {
          return _mostrarAlert(context, null);
        }
      },
    );
  }

  _mostrarAlert(BuildContext context, LoginModel dato) {
    showDialog(
        context: context,
        barrierDismissible: true,
        //mostrar dalogo
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)),
            title: Text('${dato.cliente.nombres} ${dato.cliente.apellidos}'),
            content: Column(
              mainAxisSize: MainAxisSize.min, //ajuste del tamaño al contenido
              children: [
                Text(' Celular: ${dato.cliente.celular}'),
                FlutterLogo(
                  size: 30.0,
                  colors: Colors.blue,
                )
              ],
            ),
            actions: <Widget>[
              FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text('Cancelar')),
              FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text('0k'))
            ],
          );
        });
  }

  _mostrarAlertDanger(BuildContext context, String message) {
    showDialog(
        context: context,
        barrierDismissible: true,
        //mostrar dalogo
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)),
            title: Text("No existen datos"),
            content: Column(
              mainAxisSize: MainAxisSize.min, //ajuste del tamaño al contenido
              children: [
                Text(message),
                FlutterLogo(
                  size: 30.0,
                  colors: Colors.red,
                )
              ],
            ),
            actions: <Widget>[
              FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text('Cancelar')),
              FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text('0k'))
            ],
          );
        });
  }
}
