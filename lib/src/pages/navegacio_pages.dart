import 'package:app_provider/src/widgets/button_navegacion.dart';
import 'package:flutter/material.dart';

class NavegacionPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //obtener titulo de heroes
    return Scaffold(
      appBar: AppBar(
        title: Text("Navegacion"),
      ),
      body: Center(child: ButtonNavigation()),
      //floatingActionButton: SuperFloatingAction(),
    );
  }
}
