
import 'package:flutter/material.dart';
import 'package:app_provider/src/widgets/super_floatingAction.dart';
import 'package:app_provider/src/widgets/super_text.dart';
import 'package:app_provider/src/providers/heroes_info.dart';
import 'package:provider/provider.dart';



class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //obtener titulo de heroes
    final heroesInfo=Provider.of<HeroesInfo>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(heroesInfo.heroe),
      ),
      body: Center(child: SuperText()),
      floatingActionButton: SuperFloatingAction(),
    );
  }
}
