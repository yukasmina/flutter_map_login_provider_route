import 'package:flutter/material.dart';
import 'package:app_provider/src/widgets/viajes_seguros.dart';
import 'package:app_provider/src/widgets/map_floatingAction.dart';

class MapPages extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Angel Minga"),
      ),
      body: ViajeSeguro(),
      floatingActionButton: MapFloatingAction(),
      /*body: Center(child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text("Desde: San lucas"),
          Text("Hasta: Loja"),
          FlatButton(
            child: Text("Aceptar viaje"),
            onPressed: (){},
          )
        ],
      )*/
    );
  }
}
