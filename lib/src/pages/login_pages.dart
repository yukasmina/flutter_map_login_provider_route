import 'package:flutter/material.dart';
import 'package:app_provider/src/widgets/datos_usuario.dart';

class LoginPages extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Login"),
      ),
      body: Center(
          child: Column(
        children: <Widget>[
          DatosUsuario(),
          //ButtonLogin(),
          RaisedButton(
              color: Colors.blueAccent[300],
              child: Icon(Icons.arrow_back_ios),
              onPressed: () {
                Navigator.of(context).pushNamed('nav');
              })
        ],
      )),
    );
  }
}
