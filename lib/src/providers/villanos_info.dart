import 'package:flutter/material.dart';

/*
 *notificar el cambio de nombre
 */
class VillanosInfo with ChangeNotifier {
  String _villano = "Red Skull";
  Color colorBase=Colors.blue;
  /*
   * Metodos get y set
   */
  String get villano => _villano;

  set villano(String value) {
    _villano = value;
    this.colorBase=(value=="Red Skull"?Colors.red:Colors.blue);
    notifyListeners();
  }
}