import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart' as maps;
import 'package:google_maps_webservice/directions.dart';
import 'package:http/http.dart';

/*
 *notificar el cambio de nombre
 */
class DirectionInfo with ChangeNotifier {
  GoogleMapsDirections directionsApi =
      GoogleMapsDirections(apiKey: "AIzaSyAnrgRG0E5Y-_iTtC9Olpp4rlv301dWN8E");
  Set<maps.Polyline> _route = Set();
  Set<maps.Polyline> get currentRoute => _route;

  findDirections(maps.LatLng from, maps.LatLng to, rutas) async {
    print("LLLega __________________________________________ LLega");
    /* var origin=Location(from.latitude,from.longitude);
  var destination=Location(to.latitude,to.longitude);*/
    var origin = Location(from.longitude, from.latitude);
    var destination = Location(to.longitude, to.latitude);
    var resultado =
        await directionsApi.directionsWithLocation(origin, destination);
    //___________________________________________
    String url =
        'https://api.mapbox.com/directions/v5/mapbox/${rutas}/${origin};${destination}?alternatives=true&geometries=geojson&steps=true&access_token=pk.eyJ1IjoieXVrYXNtaW5hIiwiYSI6ImNrZW04czFwdzF6Y3Uyem83azc0aTM1dHkifQ.S_8CWo-l1FkA88GguLV56A';
    //String url = 'https://api.mapbox.com/directions/v5/mapbox/driving/-79.207368,-3.999739;-79.20445,-3.99534?alternatives=true&geometries=geojson&steps=true&access_token=pk.eyJ1IjoieXVrYXNtaW5hIiwiYSI6ImNrZW04czFwdzF6Y3Uyem83azc0aTM1dHkifQ.S_8CWo-l1FkA88GguLV56A';
    //String url = 'https://api.mapbox.com/directions/v5/mapbox/driving/-79.204211%2C-3.995355%3B-79.204737%2C-3.987986?alternatives=true&geometries=geojson&steps=true&access_token=pk.eyJ1IjoieXVrYXNtaW5hIiwiYSI6ImNrZW04czFwdzF6Y3Uyem83azc0aTM1dHkifQ.S_8CWo-l1FkA88GguLV56A';
    Response response = await get(url);
    // sample info available in response
    int statusCode = response.statusCode;
    Map<String, String> headers = response.headers;
    String contentType = headers['content-type'];
    String json = response.body;
    print("Consulta servicio web______________________________________");
    Map<String, dynamic> jsonDatos = jsonDecode(json);
    var listCoordenadas = jsonDatos['routes'][0]['geometry']['coordinates'];
    List<maps.LatLng> point = [];
    for (var name in listCoordenadas) {
      point.add(new maps.LatLng(name[1], name[0]));
//   print(name[0]);
//   print(name[1]);
    }
    print(point);

    //___________________________________________
    Set<maps.Polyline> newRoute = Set();
    if (!resultado.isOkay) {
      /*var route=resultado.routes[0];//se selecciona la primera ruta
    //var route=listCoordenadas;//se selecciona la primera ruta
    var leg=route.legs[0];
    List<maps.LatLng> points=[];
    leg.steps.forEach((step) {
      points.add(maps.LatLng(step.startLocation.lat,step.startLocation.lng));
      points.add(maps.LatLng(step.endLocation.lat,step.endLocation.lng));
    });*/
      var line = maps.Polyline(
          points: point,
          polylineId: maps.PolylineId("mejor ruta"),
          color: Colors.teal,
          width: 4);
      newRoute.add(line);
      _route = newRoute;
      notifyListeners();
    } else {
      print("ERROR !!!!! ${resultado.status}");
    }
  }

  notifyListeners();
}
