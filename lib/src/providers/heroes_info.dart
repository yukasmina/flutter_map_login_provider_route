import 'package:flutter/material.dart';

/*
 *notificar el cambio de nombre
 */
class HeroesInfo with ChangeNotifier {
  String _heroe = "Angel Minga";
  Color colorBase = Colors.blue;

  /*
   * Metodos get y set
   */
  String get heroe => _heroe;

  set heroe(String value) {
    _heroe = value;
    this.colorBase = (value == "Iroman" ? Colors.red : Colors.blue);

    notifyListeners();
  }
}
