import 'dart:convert';

import 'package:app_provider/src/models/inicio_model.dart';
import 'package:http/http.dart' as http;

class InicioProvider {
  final String _url = 'http://169.62.217.189:91/c/login/autenticar';

  Future<LoginModel> consultaDatos(String usuario, String clave) async {
    print('$usuario   $clave');
    final url = '$_url';
    final resp = await http.post(url, body: {
      "idAplicativo": "1",
      "cliente": usuario,
      "clave": clave,
      "token": "s/n",
      "codigoPais": "+593",
      "imei": "bd664229-87d0-4b52-9778-5d73cf2aef68",
      "vs": "1.28.186",
      "idPlataforma": "2",
      "marca": "Google",
      "modelo": "Android SDK built for x86",
      "so": "10"
    }, headers: {
      "version": "3.1.1",
      "Content-Type": "application/x-www-form-urlencoded"
    });

    if (resp.statusCode == 200) {
      //final body = json.decode(resp.body);
      print("Existe datos");
      //final loginTemp = LoginModel.fromJson(resp.body);
      // print(loginTemp);
      return LoginModel.fromJson(resp.body);
    } else {
      print("No existe datos.......s");
      return null;
    }
  }
}
