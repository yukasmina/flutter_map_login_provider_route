import 'package:app_provider/src/providers/heroes_info.dart';
import 'package:app_provider/src/providers/villanos_info.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SuperText extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    /**
     * 2 formas de utilizar profile
     * 1 con el final
     * 2 con el widget consume de provider
     */
    final heroesInfo = Provider.of<HeroesInfo>(context);
    final villanosInfo = Provider.of<VillanosInfo>(context);
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        //debe de tener el tipo de dato
        /*Consumer<HeroesInfo>(
                    //context, variable, widges que no se usa
          builder: (context,heroesInfo,_)=> Text(heroesInfo.heroe,
              style: TextStyle(
                  fontSize: 30.0,
                  color:heroesInfo.colorBase
              ),
        ),*/
        //Text('Contenido super text')
        Text(
          heroesInfo.heroe,
          style: TextStyle(fontSize: 30.0, color: heroesInfo.colorBase),
        ),
        Text(
          villanosInfo.villano,
          style: TextStyle(fontSize: 25.0, color: villanosInfo.colorBase),
        )
      ],
    );
  }
}
