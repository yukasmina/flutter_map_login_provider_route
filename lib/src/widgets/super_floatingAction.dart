import 'package:app_provider/src/providers/heroes_info.dart';
import 'package:app_provider/src/providers/villanos_info.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SuperFloatingAction extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final heroesInfo = Provider.of<HeroesInfo>(context);
    final villanoInfo = Provider.of<VillanosInfo>(context);
    return Column(mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[
      FloatingActionButton(
        child: Icon(Icons.access_time),
        backgroundColor: Colors.red,
        onPressed: () {
          heroesInfo.heroe = "Iroman";
          villanoInfo.villano = "El Mandarin";
        },
        heroTag: 'uno',
      ),
      SizedBox(
        height: 10.0,
      ),
      FloatingActionButton(
        child: Icon(Icons.account_balance),
        backgroundColor: Colors.blue,
        onPressed: () {
          heroesInfo.heroe = "Capitan America";
          villanoInfo.villano = "Red Skull";
        },
        heroTag: 'dos',
      )
    ]);
  }
}
