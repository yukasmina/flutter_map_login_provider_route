import 'dart:math';

import 'package:app_provider/src/providers/direction_info.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';

class ViajeSeguro extends StatefulWidget {
  final LatLng fromPoint = LatLng(-3.999739, -79.207368);
  final LatLng toPoint = LatLng(-3.99534, -79.20445);
  final String rutas = "driving";
  @override
  _ViajeSeguroState createState() => _ViajeSeguroState();
}

class _ViajeSeguroState extends State<ViajeSeguro> {
  GoogleMapController _mapController;
  @override
  Widget build(BuildContext context) {
    final directionInfo = Provider.of<DirectionInfo>(context);
    return GoogleMap(
      initialCameraPosition: CameraPosition(
          //target: LatLng(-3.997407,-79.206094),//posicion
          target: widget.fromPoint, //posicion
          zoom: 16),
      //crear marcador
      markers: _createMarkers(),
      polylines: directionInfo.currentRoute,
      onMapCreated: _onMapCreated,
      myLocationEnabled: true,
      myLocationButtonEnabled: true,
    );
  }

  Set<Marker> _createMarkers() {
    var tmp = Set<Marker>();
    tmp.add(Marker(
        markerId: MarkerId("FromPoint"),
        position: widget.fromPoint,
        infoWindow: InfoWindow(title: "San lucas")));
    tmp.add(Marker(
        markerId: MarkerId("ToPoint"),
        position: widget.toPoint,
        infoWindow: InfoWindow(title: "Loja")));
    return tmp;
  }

  //verifica si el mapa se creo
  void _onMapCreated(GoogleMapController controller) {
    _mapController = controller;
    _centerView();
  }

  _centerView() async {
    //tipo asincrono verifica que el mapa ya esta creado
    await _mapController
        .getVisibleRegion(); //si esta visibl la region significa que esta cargado completo el mapa
//______________________Calculo d elos 4 puntos cardinales
    var left = min(widget.fromPoint.latitude, widget.toPoint.latitude);
    var right = max(widget.fromPoint.latitude, widget.toPoint.latitude);
    var top = max(widget.fromPoint.longitude, widget.toPoint.longitude);
    var botton = min(widget.fromPoint.longitude, widget.toPoint.longitude);
    //
    var bounds = LatLngBounds(
        southwest: LatLng(left, botton), northeast: LatLng(right, top));
    var cameraUpdate = CameraUpdate.newLatLngBounds(bounds, 50);
    //alinea la camara
    _mapController.animateCamera(cameraUpdate);
    final api = Provider.of<DirectionInfo>(context, listen: false);
    api.findDirections(widget.fromPoint, widget.toPoint, widget.rutas);
  }
}
