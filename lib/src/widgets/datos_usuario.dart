import 'package:app_provider/main.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

SharedPreferences localStorage;
TextEditingController usuarioController = new TextEditingController();
TextEditingController claveController = new TextEditingController();

class DatosUsuario extends StatefulWidget {
  static Future init() async {
    localStorage = await SharedPreferences.getInstance();
  }

  @override
  _DatosUsuarioState createState() => _DatosUsuarioState();
}

class _DatosUsuarioState extends State<DatosUsuario> {
  @override
  Widget build(BuildContext context) {
    final usuario = TextField(
      controller: usuarioController,
      //obscureText: true,
      decoration: InputDecoration(
        hintText: 'Usuario',
        border: OutlineInputBorder(),
      ),
    );
    final clave = TextField(
      controller: claveController,
      //obscureText: true,
      decoration:
          InputDecoration(border: OutlineInputBorder(), hintText: 'Clave'),
    );
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Padding(padding: EdgeInsets.only(top: 50.0)),
        Container(
          padding: EdgeInsets.only(left: 30.0, right: 30.0),
          //margin: EdgeInsets.symmetric(vertical: 10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[usuario],
          ),
        ),
        Padding(padding: EdgeInsets.only(top: 10.0)),
        Container(
          padding: EdgeInsets.only(left: 30.0, right: 30.0),
          // margin: EdgeInsets.symmetric(vertical: 10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[clave],
          ),
        ),
        Padding(padding: EdgeInsets.only(top: 30.0)),
        RaisedButton(
          onPressed: save,
          child: Text("Login"),
        ),
        Padding(padding: EdgeInsets.only(top: 30.0)),
        RaisedButton(
          onPressed: prsentar,
          child: Text("Presentar"),
        ),
        Padding(padding: EdgeInsets.only(top: 20.0)),
        if (localStorage != null)
          Text(
              "Usuario logeado !!!!  -> usuario: ${localStorage.get('usuario')}  contraseña: ${localStorage.get('clave')}")
      ],
    );
  }
}

save() async {
  await DatosUsuario.init();
  localStorage.setString('usuario', usuarioController.text.toString());
  localStorage.setString('clave', claveController.text.toString());
}

prsentar() async {
  await DatosUsuario.init();
  if (localStorage != null)
    print(
        "Usuario logeado !!!!  -> usuario: ${localStorage.get('usuario')}  contraseña: ${localStorage.get('clave')}");
}
