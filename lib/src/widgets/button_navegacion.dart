import 'package:flutter/material.dart';

class ButtonNavigation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        RaisedButton(
          onPressed: () {
            print("Home");
            Navigator.of(context).pushNamed('home');
          },
          child: Text("Provider"),
        ),
        RaisedButton(
          onPressed: () {
            //Navigator.push(context, MaterialPageRoute(builder: (context) => LoginPages()));
            print("Login");
            Navigator.of(context).pushNamed('login');
          },
          child: Text("Login"),
        ),
        RaisedButton(
          onPressed: () {
            print("Map");
            Navigator.of(context).pushNamed('map');
          },
          child: Text("MapGoogle"),
        ),
        RaisedButton(
          onPressed: () {
            print("inicio");
            Navigator.of(context).pushNamed('inicio');
          },
          child: Text("Inicio"),
        ),
      ],
    );
  }
}
