import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:app_provider/src/providers/direction_info.dart';

class MapFloatingAction extends StatelessWidget {
  final LatLng fromPoint = LatLng(-3.999739, -79.207368);
  final LatLng toPoint = LatLng(-3.99534, -79.20445);

  @override
  Widget build(BuildContext context) {
    final directionInfo = Provider.of<DirectionInfo>(context);
    return Row(mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[
      FloatingActionButton(
        child: Icon(Icons.drive_eta),
        backgroundColor: Colors.green,
        onPressed: () {
          directionInfo.findDirections(fromPoint, toPoint, "driving");
        },
        heroTag: 'uno',
      ),
      SizedBox(
        width: 10.0,
      ),
      FloatingActionButton(
        child: Icon(Icons.transfer_within_a_station),
        backgroundColor: Colors.teal,
        onPressed: () {
          directionInfo.findDirections(fromPoint, toPoint, "walking");
        },
        heroTag: 'dos',
      ),
      SizedBox(
        width: 10.0,
      ),
      FloatingActionButton(
        child: Icon(Icons.motorcycle),
        backgroundColor: Colors.lightBlue,
        onPressed: () {
          directionInfo.findDirections(fromPoint, toPoint, "cycling");
        },
        heroTag: 'tres',
      ),
      SizedBox(
        width: 70.0,
      ),
    ]);
  }
}
