import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:app_provider/src/pages/home_pages.dart';
import 'package:app_provider/src/pages/navegacio_pages.dart';
import 'package:app_provider/src/providers/heroes_info.dart';
import 'package:app_provider/src/providers/villanos_info.dart';
import 'package:app_provider/src/pages/iniciar_pages.dart';
import 'package:app_provider/src/pages/login_pages.dart';
import 'package:app_provider/src/pages/map_pages.dart';
import 'package:app_provider/src/providers/direction_info.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    //trabaja con estado para notificar ,, utiliza builder
    /*return ChangeNotifierProvider(
        create: (context)=>HeroesInfo(),
         child: MaterialApp(
            title: 'Material app',
            initialRoute: 'home',
            routes: {
              'home': (context)=>HomePage()
            },
        ));*/
    //Para usar varios provider
    /* return MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (context)=>HeroesInfo(),),
          ChangeNotifierProvider(create: (context)=>VillanosInfo(),),
          ChangeNotifierProvider(create: (context)=>DirectionInfo(),),
        ],
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Material app',
          initialRoute: 'home',
          routes: {
            'home': (context)=>MapPages()
          },
        ));*/
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (context) => HeroesInfo(),
          ),
          ChangeNotifierProvider(
            create: (context) => VillanosInfo(),
          ),
          ChangeNotifierProvider(
            create: (context) => DirectionInfo(),
          ),
        ],
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Material app',
          initialRoute: 'nav',
          routes: {
            'nav': (_) => NavegacionPage(),
            'home': (_) => HomePage(),
            'login': (_) => LoginPages(),
            'map': (_) => MapPages(),
            'inicio': (_) => IniciarPages(),
          },
        ));
  }
}
